﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Employee
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        ObservableCollection<Employee> employees = new ObservableCollection<Employee>();
        public ObservableCollection<Employee> Employees
        {
            get { return employees; }
            set
            {
                if(employees != value)
                {
                    employees = value;
                    OnPropertyChanged();
                }
            }
        }

        int ind;
        public int index
        {
            get { return ind; } set { ind = value; }
        }

        Employee employee;
        public Employee Employee
        {
            get { return employee; }
            set { employee = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            AddFrame frame = new AddFrame(this.Employees);
            frame.ShowDialog();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            Employees.Remove(Employees[employeeList.SelectedIndex]);
        }
    }
}
