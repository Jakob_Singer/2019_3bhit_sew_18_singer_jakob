﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_Employee
{
    /// <summary>
    /// Interaktionslogik für AddFrame.xaml
    /// </summary>
    public partial class AddFrame : Window
    {
        ObservableCollection<Employee> employees;
        public AddFrame(ObservableCollection<Employee> employees)
        {
            InitializeComponent();
            this.employees = employees;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            employees.Add(new Employee(txtID.Text, txtName.Text, txtDepartment.Text,
                txtEmail.Text, txtEmail.Text, txtSalary.Text));
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
