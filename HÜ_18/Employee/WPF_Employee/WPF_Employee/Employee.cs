﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WPF_Employee
{
    public class Employee : INotifyPropertyChanged
    {
        public string memberID;
        public string name;
        public string department;
        public string phone;
        public string email;
        public string salary;

        public string MemberID
        {
            get { return memberID; }
            set
            {
                if(value != memberID)
                {
                    memberID = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Department
        {
            get { return department; }
            set
            {
                if (value != department)
                {
                    department = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Phone
        {
            get { return phone; }
            set
            {
                if (value != phone)
                {
                    phone = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Email
        {
            get { return email; }
            set
            {
                if (value != email)
                {
                    email = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Salary
        {
            get { return salary; }
            set
            {
                if (value != salary)
                {
                    salary = value;
                    OnPropertyChanged();
                }
            }
        }

        public Employee(string memberID, string name, string department, string phone,
            string email, string salary)
        {
            this.MemberID = memberID;
            this.Name = name;
            this.Department = department;
            this.Phone = phone;
            this.Email = email;
            this.Salary = salary;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
