﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace _20MMTT_WPF_Process
{
    class ProcessMVC : INotifyPropertyChanged
    {
        ObservableCollection<Process> processes = new ObservableCollection<Process>();
        public ObservableCollection<Process> Processes
        {
            get
            {
                return processes;
            }
            set
            {
                if(processes != value)
                {
                    processes = value;
                    OnPropertyChanged();
                }
            }
        }

        #region Events
        public void AddProcess(Process process)
        {
            Processes.Add(process);
        }
        public void DeleteProcess(Process process)
        {
            Processes.Remove(process);
        }
        public void ClearProcesses()
        {
            Processes = new ObservableCollection<Process>();
        }
        #endregion

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
