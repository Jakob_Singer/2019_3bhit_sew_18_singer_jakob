﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _20MMTT_WPF_Process
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ProcessMVC mVC = new ProcessMVC();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = mVC;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // load CSV
            try
            {
                using(StreamReader sr = new StreamReader("config.CSV"))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] properties = sr.ReadLine().Split(';');
                        mVC.AddProcess(new Process(properties[0], properties[1],
                            (properties[2] == "True"), (properties[3] == "True"), properties[4],
                            properties[5], properties[6]));
                    }

                    sr.Close();
                }
            } catch { }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // save CSV
            using (StreamWriter sw = new StreamWriter("config.CSV"))
            {
                foreach (Process process in mVC.Processes)
                {
                    sw.WriteLine($"{process.Id};{process.Importance};{process.Sheduled};" +
                       $"{process.ThreadSafe};{process.Description};{process.Progress};" +
                       $"{process.State}");
                }

                sw.Close();
            }
        }

        #region Events

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            string state = "";
            state = (rbRunning.IsChecked == true) ? "Running" : (rbReady.IsChecked == true) ?
                "Ready" : "Blocked";

            if (!String.IsNullOrEmpty(txtId.Text.Trim()) &&
                    !String.IsNullOrEmpty(txtImportance.Text.Trim()) &&
                    !String.IsNullOrEmpty(txtDescription.Text.Trim()) &&
                    !String.IsNullOrEmpty(txtProgress.Text.Trim()) &&
                    !String.IsNullOrEmpty(state))
                mVC.AddProcess(new Process(txtId.Text, txtImportance.Text, cbSheduled.IsChecked == true, 
                    cbThreadSafe.IsChecked == true, txtDescription.Text, txtProgress.Text, state));
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            mVC.ClearProcesses();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try { mVC.DeleteProcess((Process)processList.Items[processList.SelectedIndex]); }
            catch { MessageBox.Show("Select a item first to delete it!"); }
        }

        #endregion
    }
}
