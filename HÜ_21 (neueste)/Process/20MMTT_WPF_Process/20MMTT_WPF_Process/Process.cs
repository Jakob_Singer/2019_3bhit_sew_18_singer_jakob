﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20MMTT_WPF_Process
{
    class Process
    {
        public Process(string id, string importance, bool sheduled, bool threadSafe,
            string description, string progress, string state)
        {
            this.id = id;
            this.importance = importance;
            this.sheduled = sheduled;
            this.threadSafe = threadSafe;
            this.description = description;
            this.progress = progress;
            this.state = state;
        }

        string id;
        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        string importance;
        public string Importance
        {
            get { return importance; }
            set { importance = value; }
        }
        bool sheduled;
        public bool Sheduled
        {
            get { return sheduled; }
            set { sheduled = value; }
        }
        bool threadSafe;
        public bool ThreadSafe
        {
            get { return threadSafe; }
            set { threadSafe = value; }
        }
        string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        string progress;
        public string Progress
        {
            get { return progress; }
            set { progress = value; }
        }
        string state;
        public string State
        {
            get { return state; }
            set { state = value; }
        }
    }
}
