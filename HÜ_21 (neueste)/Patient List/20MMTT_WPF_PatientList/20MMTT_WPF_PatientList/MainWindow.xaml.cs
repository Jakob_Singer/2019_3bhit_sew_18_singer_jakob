﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _20MMTT_WPF_PatientList
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;
        }

        Patient selectedPatient;
        public Patient SelectedPatient
        {
            get { return selectedPatient; }
            set { selectedPatient = value; }
        }

        ObservableCollection<Patient> patients = new ObservableCollection<Patient>();
        public ObservableCollection<Patient> Patients
        {
            get { return patients; }
            set { patients = value; }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Patients.Add(new Patient(txtId.Text, txtName.Text, txtPhone.Text));
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            Patients.Remove((Patient)patientList.Items[patientList.SelectedIndex]);
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchId = txtId.Text;

            foreach (Patient patient in Patients)
                if (patient.Id == searchId)
                    patientList.SelectedIndex = Convert.ToInt32(searchId) - 1;
        }
    }
}
