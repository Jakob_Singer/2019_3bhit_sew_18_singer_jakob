﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20MMTT_WPF_PatientList
{
    public class Patient
    {
        public Patient(string id, string name, string mobileNumber)
        {
            this.id = id;
            this.name = name;
            this.mobileNumber = mobileNumber;
        }

        string id;
        public string Id
        {
            get { return id;  }
            set { id = value; }
        }

        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        string mobileNumber;
        public string MobileNumber
        {
            get { return mobileNumber; }
            set { mobileNumber = value; }
        }

        public override string ToString()
        {
            return $"The Patient {Name} has the id {Id} and the phone Number {mobileNumber}.";
        }
    }
}
