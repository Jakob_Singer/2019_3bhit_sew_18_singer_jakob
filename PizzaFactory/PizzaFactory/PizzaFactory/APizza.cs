﻿using System;

namespace PizzaFactory.PizzaFactory
{
    class APizza
    {
        public virtual void prepare() { Console.WriteLine("The pizza is beeing prepared."); }
        public virtual void bake() { Console.WriteLine("The pizza is beeing baked."); }
        public virtual void cut() { Console.WriteLine("The pizza is beeing cut."); }
        public virtual void box() { Console.WriteLine("The pizza is beeing boxed."); }
    }
}
