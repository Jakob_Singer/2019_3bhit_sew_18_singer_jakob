﻿namespace PizzaFactory.PizzaFactory
{
    class PizzaStore
    {
        APizza pizza;

        /// createPizza ///
        /// - not case sensitive
        /// 
        /// Parameters
        /// [0 - location]: specifies store
        /// values possible:
        /// newyork
        /// chicago
        /// 
        /// [1 - type]: specifies type of pizza
        /// values possible:
        /// cheese,
        /// pepperoni,
        /// clam,
        /// veggie
        //
        /// Returns:
        /// Pizza that corresponds the type and location
        public APizza CreatePizza(string location, string type)
        {
            location = location.ToLower();
            type = type.ToLower();

            switch(location)
            {
                case "newyork":
                    pizza = Locations.NYStylePizzaStore.CreatePizza(type);
                    break;
                case "chicago":
                    pizza = Locations.ChicagoStylePizzaStore.CreatePizza(type);
                    break;
                default:
                    throw new UnknownLocationException();
            }

            return pizza;
        }
        public APizza OrderPizza()
        {
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();

            return pizza;
        }
    }
}
