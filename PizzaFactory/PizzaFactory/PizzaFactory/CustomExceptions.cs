﻿using System;

namespace PizzaFactory.PizzaFactory
{
    class UnknownPizzaException : Exception
    {
        public UnknownPizzaException() : base("unkown pizza!") { }
    }
    class UnknownLocationException : Exception
    {
        public UnknownLocationException() : base("unkown location!") { }
    }
}
