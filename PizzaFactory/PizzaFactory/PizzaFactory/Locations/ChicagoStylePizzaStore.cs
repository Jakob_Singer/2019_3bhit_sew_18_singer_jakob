﻿using ChicagoStyle = PizzaFactory.PizzaFactory.Pizzas.ChicagoStylePizzas;

namespace PizzaFactory.PizzaFactory.Locations
{
    static class ChicagoStylePizzaStore
    {
        static APizza pizza;

        /// createPizza ///
        /// - not case sensitive
        /// 
        /// Parameters
        /// [0 - type]: specifies type of pizza
        /// values possible:
        /// cheese,
        /// pepperoni,
        /// clam,
        /// veggie
        //
        /// Returns:
        /// Pizza that corresponds the type
        public static APizza CreatePizza(string type)
        {
            type = type.ToLower();

            switch (type)
            {
                case "cheese":
                    pizza = new ChicagoStyle.ChicagoStyleCheesePizza();
                    break;
                case "pepperoni":
                    pizza = new ChicagoStyle.ChicagoStyleCheesePizza();
                    break;
                case "clam":
                    pizza = new ChicagoStyle.ChicagoStyleCheesePizza();
                    break;
                case "veggie":
                    pizza = new ChicagoStyle.ChicagoStyleCheesePizza();
                    break;
                default:
                    throw new UnknownPizzaException();
            }

            return pizza;
        }
    }
}
