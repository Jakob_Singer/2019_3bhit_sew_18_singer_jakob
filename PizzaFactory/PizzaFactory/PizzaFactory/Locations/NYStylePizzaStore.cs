﻿using System;
using System.Collections.Generic;
using System.Linq;
using NYStyle = PizzaFactory.PizzaFactory.Pizzas.NYStylePizzas;

namespace PizzaFactory.PizzaFactory.Locations
{
    static class NYStylePizzaStore
    {
        static APizza pizza;

        /// createPizza ///
        /// - not case sensitive
        /// 
        /// Parameters
        /// [0 - type]: specifies type of pizza
        /// values possible:
        /// cheese,
        /// pepperoni,
        /// clam,
        /// veggie
        //
        /// Returns:
        /// Pizza that corresponds the type
        public static APizza CreatePizza(string type)
        {
            type = type.ToLower();

            switch (type)
            {
                case "cheese":
                    pizza = new NYStyle.NYStyleCheesePizza();
                    break;
                case "pepperoni":
                    pizza = new NYStyle.NYStyleCheesePizza();
                    break;
                case "clam":
                    pizza = new NYStyle.NYStyleCheesePizza();
                    break;
                case "veggie":
                    pizza = new NYStyle.NYStyleCheesePizza();
                    break;
                default:
                    throw new UnknownPizzaException();
            }

            return pizza;
        }
    }
}
