﻿using PizzaFactory.PizzaFactory;
using PizzaFactory.PizzaFactory.Locations;
using PizzaFactory.PizzaFactory.Pizzas;
using PizzaFactory.PizzaFactory.Pizzas.ChicagoStylePizzas;
using PizzaFactory.PizzaFactory.Pizzas.NYStylePizzas;

namespace PizzaFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            PizzaStore store = new PizzaStore();

            APizza NYCheesePizza = store.CreatePizza("neWyORk", "ChEEsE");
            store.OrderPizza();

            System.Console.WriteLine("\ntype:");
            System.Console.WriteLine(NYCheesePizza.GetType());
        }
    }
}
