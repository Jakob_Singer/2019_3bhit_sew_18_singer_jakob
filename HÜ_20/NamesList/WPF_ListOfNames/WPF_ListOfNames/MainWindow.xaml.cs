﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_ListOfNames
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;
        }

        ObservableCollection<NameObj> names = new ObservableCollection<NameObj>();
        public ObservableCollection<NameObj> Names
        {
            get { return names; }
        }

        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            names.Add(new NameObj(txtName.Text));
        }
    }
}
