﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_ListOfNames
{
    public class NameObj
    {
        public string Name
        {
            get; set;
        }

        public NameObj(string name)
        {
            this.Name = name;
        }
    }
}
