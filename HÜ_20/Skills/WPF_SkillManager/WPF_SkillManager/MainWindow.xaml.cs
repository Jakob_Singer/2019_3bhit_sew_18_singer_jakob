﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_SkillManager
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SkillVM vm;
        public MainWindow()
        {
            InitializeComponent();

            vm = new SkillVM();
            DataContext = vm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            vm.AddProgress(10);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            vm.AddProgress(-10);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            vm.AddSkill();
        }
    }
}
