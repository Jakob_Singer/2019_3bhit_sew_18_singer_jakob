﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WPF_SkillManager
{
    class SkillVM
    {
        private string name;
        public string Name
        {
            get => name;
            set { name = value; }
        }

        ObservableCollection<StackPanel> skills = new ObservableCollection<StackPanel>();
        public ObservableCollection<StackPanel> Skills
        {
            get => skills;
            set { skills = value; }
        }

        StackPanel selectedSkill;
        public StackPanel SelectedSkill
        {
            get => selectedSkill;
            set
            {
                selectedSkill = value;
            }
        }

        StackPanel GetSkill(string name)
        {
            StackPanel SkillPanel = new StackPanel() { Orientation = Orientation.Horizontal };
            Label Name = new Label() { Content = name };
            ProgressBar Progress = new ProgressBar() { Width = 200, Height = 20 };

            SkillPanel.Children.Add(Name);
            SkillPanel.Children.Add(Progress);

            return SkillPanel;
        }

        public void AddSkill()
        {
            Skills.Add(GetSkill(Name));
        }
        public void AddProgress(int progress)
        {
            if (SelectedSkill != null)
                ((ProgressBar)(SelectedSkill.Children[1])).Value += progress;
            else
                MessageBox.Show("Select a Skill first!");
        }
    }
}
