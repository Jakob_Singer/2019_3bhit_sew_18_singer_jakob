﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WPF_Student_SubjectList
{
    class Student {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Age { get; set; }
        public string Class { get; set; }

        public Student(string firstname, string lastname, string age,
            string clazz)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Age = age;
            this.Class = clazz;
        }
    }
}
