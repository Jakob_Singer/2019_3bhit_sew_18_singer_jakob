﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WPF_Student_SubjectList.Commands;

namespace WPF_Student_SubjectList.ViewModels
{
    class StudentViewModel
    {
        public ObservableCollection<Student> Students { get; set; }

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Age { get; set; }
        string clazz;
        public string Class { get { return clazz; }
            set {
                clazz = (value.Contains("System.Windows.Controls.ComboBoxItem:")) ?
                    value.Split(':')[1] : value;
            }
        }

        public AddCommand AddStudentCommand { get; private set; }
        public DeleteCommand DeleteStudentCommand { get; private set; }
        public LoadCommand LoadStudentCommand { get; private set; }

        public StudentViewModel()
        {
            AddStudentCommand = new AddCommand(AddStudent);
            DeleteStudentCommand = new DeleteCommand(DeleteStudent);
            LoadStudentCommand = new LoadCommand(LoadStudent);

            Students = new ObservableCollection<Student>();
        }

        public void AddStudent()
        {
            if (!String.IsNullOrEmpty(Firstname) &&
                !String.IsNullOrEmpty(Lastname) &&
                !String.IsNullOrEmpty(Age) &&
                !String.IsNullOrEmpty(Class))
            {
                Students.Add(new Student(Firstname, Lastname, Age, Class));
                foreach (TextBox item in MainWindow.txtStudents)
                    item.Clear();
            }
            else
                MessageBox.Show("The fields mustn't be empty!", "Attention!");
        }

        public void DeleteStudent()
        {
            try {
                if(MessageBox.Show("R u sure that u wanna delete dis?", 
                    "Attention!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    Students.Remove(Students[MainWindow.StudentList.SelectedIndex]);
            } catch
            {
                MessageBox.Show("Select a Student first!", "Error");
            }
}

        public void LoadStudent()
        {
            try
            {
                Student selectedStudent = (Student)MainWindow.StudentList.SelectedItem;

                MessageBox.Show($"Firstname: {selectedStudent.Firstname}" +
                    $"\nLastname: {selectedStudent.Lastname}\nAge: {selectedStudent.Age}" +
                    $"\nClass: {selectedStudent.Class}", "Student " + selectedStudent.Firstname);
            } catch
            {
                MessageBox.Show("Select a Student first!", "Error");
            }
        }
    }
}
