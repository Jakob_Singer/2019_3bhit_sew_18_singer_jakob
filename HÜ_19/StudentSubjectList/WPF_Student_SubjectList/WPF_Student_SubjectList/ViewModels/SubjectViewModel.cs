﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WPF_Student_SubjectList.Commands;

namespace WPF_Student_SubjectList.ViewModels
{
    class SubjectViewModel
    {
        public ObservableCollection<Subject> Subjects { get; set; }

        public string Shortcut { get; set; }
        public string Name { get; set; }
        public string Difficulty { get; set; }
        public bool Favorite { get; set; }

        public AddCommand AddSubjectCommand { get; private set; }
        public DeleteCommand DeleteSubjectCommand { get; private set; }
        public LoadCommand LoadSubjectCommand { get; private set; }

        public SubjectViewModel()
        {
            AddSubjectCommand = new AddCommand(AddSubject);
            DeleteSubjectCommand = new DeleteCommand(DeleteSubject);
            LoadSubjectCommand = new LoadCommand(LoadSubject);

            Subjects = new ObservableCollection<Subject>();

            Favorite = false;
        }

        public void AddSubject()
        {
            if (!String.IsNullOrEmpty(Shortcut) &&
                !String.IsNullOrEmpty(Name) &&
                !String.IsNullOrEmpty(Difficulty))
            {
                Subjects.Add(new Subject(Shortcut, Name, Difficulty, Favorite));
                foreach (TextBox item in MainWindow.txtSubjects)
                    item.Clear();
            }
            else
                MessageBox.Show("The fields mustn't be empty!", "Attention!");
        }

        public void DeleteSubject()
        {
            try
            {
                if (MessageBox.Show("R u sure that u wanna delete dis?",
                    "Attention!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    Subjects.Remove(Subjects[MainWindow.SubjectList.SelectedIndex]);
            }
            catch
            {
                MessageBox.Show("Select a Subject first!", "Error");
            }
        }

        public void LoadSubject()
        {
            try
            {
                Subject selectedSubject = (Subject)MainWindow.SubjectList.SelectedItem;

                MessageBox.Show($"Shortcut: {selectedSubject.Shortcut}" +
                    $"\nName: {selectedSubject.Name}\nDifficulty: {selectedSubject.Difficulty}" +
                    $"\nFavorite?: {selectedSubject.Favorite}", "Subject: " + selectedSubject.Name);
            }
            catch
            {
                MessageBox.Show("Select a Subject first!", "Error");
            }
        }
    }
}
