﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_Student_SubjectList.Commands;
using WPF_Student_SubjectList.ViewModels;

namespace WPF_Student_SubjectList
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static DataGrid StudentList;
        public static DataGrid SubjectList;
        public static List<TextBox> txtStudents = new List<TextBox>();
        public static List<TextBox> txtSubjects = new List<TextBox>();

        public MainWindow()
        {
            InitializeComponent();
            InitializeComponents();

            StudentList = studentList;
            SubjectList = subjectList;
            txtStudents.AddRange(new TextBox[] { txtFirstname, txtLastname, txtAge });
            txtSubjects.AddRange(new TextBox[] { txtShortcut, txtName });
        }

        private void DifficultyChange(object s, EventArgs e)
        {
            RadioButton sender = (RadioButton)s;

            vmSubject.Difficulty = sender.Content.ToString();
        }

        private void ChangeFavorite(object sender, RoutedEventArgs e)
        {
            vmSubject.Favorite = (vmSubject.Favorite) ? false : true;
        }
    }
}
