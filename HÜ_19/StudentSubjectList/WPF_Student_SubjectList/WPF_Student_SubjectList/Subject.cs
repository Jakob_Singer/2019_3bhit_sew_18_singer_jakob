﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WPF_Student_SubjectList
{
    class Subject
    {
        public string Shortcut { get; set; }
        public string Name { get; set; }
        public string Difficulty { get; set; }
        public bool Favorite { get; set; }

        public Subject(string shortcut, string name, string difficulty,
            bool favorite)
        {
            Shortcut = shortcut;
            Name = name;
            Difficulty = difficulty;
            Favorite = favorite;
        }
    }
}
