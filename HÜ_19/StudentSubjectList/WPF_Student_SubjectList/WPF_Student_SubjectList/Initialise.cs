﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using WPF_Student_SubjectList.ViewModels;

namespace WPF_Student_SubjectList
{
    public partial class MainWindow
    {
        SubjectViewModel vmSubject;
        StudentViewModel vmStudent;

        public void InitializeComponents()
        {
            imgLogo1.Source = new BitmapImage(new Uri("Logo.png", UriKind.Relative));
            imgLogo2.Source = new BitmapImage(new Uri("Logo.png", UriKind.Relative));

            SubjectViewModel vmSubject = new SubjectViewModel();

            this.vmSubject = vmSubject;
            subjectTab.DataContext = vmSubject;

            StudentViewModel vmStudent = new StudentViewModel();

            this.vmStudent = vmStudent;
            studentTab.DataContext = vmStudent;
        }
    }
}
