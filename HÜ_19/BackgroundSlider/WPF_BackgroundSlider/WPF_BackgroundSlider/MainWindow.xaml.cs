﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_BackgroundSlider
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        int red;
        public int R
        {
            get { return red; }
            set
            {
                red = value;
                ColorChange();
            }
        }

        int green;
        public int G
        {
            get { return green; }
            set
            {
                green = value;
                ColorChange();
            }
        }
        int blue;
        public int B
        {
            get { return blue; }
            set
            {
                blue = value;
                ColorChange();
            }
        }

        void ColorChange()
        {
            this.Background = new SolidColorBrush(Color.FromRgb(Convert.ToByte(R), Convert.ToByte(G), 
                Convert.ToByte(B)));
        }
    }
}
