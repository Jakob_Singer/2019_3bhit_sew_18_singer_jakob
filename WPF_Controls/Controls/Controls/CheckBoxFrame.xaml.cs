﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Controls
{
    /// <summary>
    /// Interaktionslogik für CheckBoxFrame.xaml
    /// </summary>
    public partial class CheckBoxFrame : Window
    {
        public CheckBoxFrame()
        {
            InitializeComponent();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            cbOption1.IsChecked = true;
            cbOption2.IsChecked = true;
            cbOption3.IsChecked = true;
        }
    }
}
