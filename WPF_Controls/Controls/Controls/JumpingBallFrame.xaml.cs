﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Controls
{
    /// <summary>
    /// Interaktionslogik für JumpingBallFrame.xaml
    /// </summary>
    public partial class JumpingBallFrame : Window
    {
        public JumpingBallFrame()
        {
            InitializeComponent();
            timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 10 ) };
            timer.Tick += timer_Tick;
        }

        DispatcherTimer timer;
        Vector speed = new Vector(1, 1);
        Vector velocity = new Vector(2, 0);
        int score = 0;

        private void timer_Tick(object sender, EventArgs e)
        {
            if (velocity.Y < 2)
                velocity.Y += 0.1;

            Point newPos = new Point(BallX, BallY);

            newPos.Y += speed.Y * velocity.Y;
            newPos.X += speed.X * velocity.X;

            SetPos(newPos.X, newPos.Y);
            

            if (BallY + elBall.Height >= 360)
            {
                velocity.Y = -4;
            }
            if(BallX + elBall.Width > cnvPlayground.Width - 10 || BallX < 10)
            {
                speed.X = -speed.X;
            }
        }

        double BallX
        {
            get { return Canvas.GetLeft(elBall); }
        }
        double BallY
        {
            get { return Canvas.GetTop(elBall); }
        }
        void SetPos(double x, double y)
        {
            Canvas.SetLeft(elBall, x);
            Canvas.SetTop(elBall, y);
        }

        private void cbSpeed_Click(object sender, RoutedEventArgs e)
        {
            Vector currentSpeed = speed;

            if ((bool)cbSpeed.IsChecked) 
                speed = new Vector(currentSpeed.X * 2, currentSpeed.Y * 2);
            else
                speed = new Vector(currentSpeed.X / 2, currentSpeed.Y / 2);
        }

        private void btnPower_Click(object sender, RoutedEventArgs e)
        {
            timer.IsEnabled = !timer.IsEnabled;
        }

        private void chooseColor(object s, RoutedEventArgs e)
        {
            RadioButton sender = (RadioButton)s;

            switch (sender.Content)
            {
                case "rot":
                    elBall.Fill = Brushes.DarkRed;
                    break;
                case "blau":
                    elBall.Fill = Brushes.DarkBlue;
                    break;
                case "grün":
                    elBall.Fill = Brushes.DarkGreen;
                    break;
            }
        }

        private void elBall_Click(object sender, MouseButtonEventArgs e)
        {
            if(timer.IsEnabled)
            {
                score += (Int32)(100 * Math.Abs(speed.X));

                lblScore.Content = "score: " + score.ToString();
            }
        }
    }
}
