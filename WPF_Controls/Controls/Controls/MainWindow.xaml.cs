﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Controls
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnMessagegBox_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("An error occurred!", "Error", MessageBoxButton.OKCancel, MessageBoxImage.Error)
                == MessageBoxResult.Cancel)
                MessageBox.Show("Error has been canceled! Makes no sense but nevermind.");
        }

        private void btnTextBlock_Click(object sender, RoutedEventArgs e)
        {
            TextBlockFrame window = new TextBlockFrame();
            window.ShowDialog();
        }

        private void btnColoredTextBlock_Click(object sender, RoutedEventArgs e)
        {
            ColoredTextBlockFrame window = new ColoredTextBlockFrame();
            window.ShowDialog();
        }

        private void btnLabel_Click(object sender, RoutedEventArgs e)
        {
            LabelFrame window = new LabelFrame();
            window.ShowDialog();
        }

        private void btnTextBox_Click(object sender, RoutedEventArgs e)
        {
            TextBoxFrame window = new TextBoxFrame();
            window.ShowDialog();
        }

        private void btnCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBoxFrame window = new CheckBoxFrame();
            window.ShowDialog();
        }

        private void btnRadioButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButtonFrame window = new RadioButtonFrame();
            window.ShowDialog();
        }

        private void btnCanvasPanel_Click(object sender, RoutedEventArgs e)
        {
            CanvasPanelFrame window = new CanvasPanelFrame();
            window.ShowDialog();
        }

        private void btnWrapPanels_Click(object sender, RoutedEventArgs e)
        {
            WrapPanelFrame window = new WrapPanelFrame();
            window.ShowDialog();
        }

        private void btnStackPanels_Click(object sender, RoutedEventArgs e)
        {
            StackPanelFrame window = new StackPanelFrame();
            window.ShowDialog();
        }

        private void btnDockPanels_Click(object sender, RoutedEventArgs e)
        {
            DockPanelFrame window = new DockPanelFrame();
            window.ShowDialog();
        }

        private void btnGrid_Click(object sender, RoutedEventArgs e)
        {
            GridFrame window = new GridFrame();
            window.ShowDialog();
        }

        private void btnTabControl_Click(object sender, RoutedEventArgs e)
        {
            TabControlFrame window = new TabControlFrame();
            window.ShowDialog();
        }

        private void btnJumpingBall_Click(object sender, RoutedEventArgs e)
        {
            JumpingBallFrame window = new JumpingBallFrame();
            window.ShowDialog();
        }
    }
}
