﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Controls
{
    class Timer
    {
        public delegate void TimerTickEventHandler(object sender, EventArgs args);
        public event TimerTickEventHandler OnTick;

        Thread timerLoop;
        int interval = 1000;
        bool isRunning = false;

        public int Interval
        {
            get { return interval; }
            set { interval = value; }
        }
        public bool IsRunning
        {
            get { return isRunning; }
        }
    
        public Timer() { timerLoop = new Thread(Loop); }
        public Timer(int interval) : this()
        {
            this.interval = interval;
        }

        public void Start()
        {
            timerLoop.Start();
            isRunning = true;
        }
        public void Stop()
        {
            timerLoop.Abort();
            isRunning = false;
        }

        void Loop()
        {
            while (true)
            {
                Tick();
                Thread.Sleep(interval);
            }
        }

        void Tick()
        {
            OnTick?.Invoke(this, EventArgs.Empty);
        }
    }
}
